use twitch_clip_rust::{get_stream_duration, StreamDurationError};

#[tokio::test]
async fn test_get_stream_duration() {
    let result = get_stream_duration("123456789", "some_streamer").await;
    assert!(result.is_ok() || matches!(result, Err(StreamDurationError::DurationNotFound)));
}

#[tokio::test]
async fn test_get_stream_duration_invalid_input() {
    let result = get_stream_duration("", "").await;
    assert!(matches!(result, Err(StreamDurationError::DurationNotFound)));
}
