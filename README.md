This is a Rust project that search Twitch clips from a specified stream.

## Usage

To use this project, follow these steps:

![](example.webp)

Search for clips:
https://twitchtracker.com/<streamer>/streams/<stream_id>


Please enter the streamer's username: your_streamer_username
Please enter the VOD ID: your_vod_id


## Features

- Parallel downloading of clips using multiple workers
- Progress bar and logging for download status

## License

This project is licensed under the MIT License.