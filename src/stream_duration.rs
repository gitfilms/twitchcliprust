use reqwest::Client;
use select::document::Document;
use select::predicate::{Class, Name, Predicate};
use thiserror::Error;

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_parse_duration() {
        let html = r#"<div class="g-x-s-value">120</div>"#;
        let document = Document::from(html);
        let result = document
            .find(Class("g-x-s-value").and(Name("div")))
            .next()
            .and_then(|node| node.text().trim().parse::<i32>().ok())
            .map(|time_text| (time_text * 60) + 30);
        assert_eq!(result, Some(7230));
    }
}

/// Errors that can occur when fetching stream duration.
#[derive(Debug, Error)]
pub enum StreamDurationError {
    /// HTTP request failed.
    #[error("HTTP request failed: {0}")]
    RequestFailed(#[from] reqwest::Error),

    /// Failed to parse the duration from the response.
    #[error("Failed to parse duration: {0}")]
    ParseError(#[from] std::num::ParseIntError),

    /// Duration information was not found in the response.
    #[error("Duration not found")]
    DurationNotFound,
}

/// Fetches the duration of a Twitch stream.
///
/// # Arguments
///
/// * `stream_id` - The ID of the stream.
/// * `streamer` - The username of the streamer.
///
/// # Returns
///
/// Returns the duration of the stream in seconds, or an error if the duration
/// could not be fetched or parsed.
///
/// # Errors
///
/// This function will return an error if:
/// * The HTTP request fails
/// * The duration cannot be parsed from the response
/// * The duration information is not found in the response
pub async fn get_stream_duration(
    stream_id: &str,
    streamer: &str,
) -> Result<i32, StreamDurationError> {
    if stream_id.is_empty() || streamer.is_empty() {
        return Err(StreamDurationError::DurationNotFound);
    }

    let url = format!(
        "https://twitchtracker.com/{}/streams/{}",
        streamer, stream_id
    );

    let client = Client::new();
    let resp = client.get(&url).send().await?.text().await?;
    let document = Document::from(resp.as_str());

    document
        .find(Class("g-x-s-value").and(Name("div")))
        .next()
        .ok_or(StreamDurationError::DurationNotFound)
        .and_then(|node| {
            let time_text = node.text().trim().parse::<i32>()?;
            Ok((time_text * 60) + 30)
        })
}
