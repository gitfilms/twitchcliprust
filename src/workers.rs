use std::future::Future;
use thiserror::Error;
use tokio::sync::mpsc;

#[derive(Debug, Error)]
pub enum WorkerError<E> {
    #[error("Task error: {0}")]
    TaskError(E),
    #[error("Channel send error")]
    ChannelSendError,
}

pub struct Workers<E> {
    count: usize,
    tx: mpsc::UnboundedSender<Result<(), E>>,
    rx: mpsc::UnboundedReceiver<Result<(), E>>,
}

impl<E: Send + 'static> Workers<E> {
    pub fn new() -> Self {
        let (tx, rx) = mpsc::unbounded_channel();
        Workers { count: 0, tx, rx }
    }

    pub fn spawn<F>(&mut self, task: F)
    where
        F: Future<Output = Result<(), E>> + Send + 'static,
    {
        let tx = self.tx.clone();
        tokio::spawn(async move {
            let res = task.await;
            if tx.send(res).is_err() {
                eprintln!("Failed to send task result");
            }
        });
        self.count += 1;
    }

    pub async fn run(mut self) -> Result<(), WorkerError<E>> {
        std::mem::drop(self.tx);
        let mut completed = 0;

        while let Some(result) = self.rx.recv().await {
            completed += 1;
            match result {
                Ok(()) => {
                    if completed == self.count {
                        break;
                    }
                }
                Err(e) => return Err(WorkerError::TaskError(e)),
            }
        }

        if completed != self.count {
            Err(WorkerError::ChannelSendError)
        } else {
            Ok(())
        }
    }
}
