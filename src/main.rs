use crate::stream_duration::get_stream_duration;
use crate::stream_duration::StreamDurationError;
use crate::workers::Workers;
use env_logger::{Builder, Env};
use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use indicatif_log_bridge::LogWrapper;
use log::{debug, error, info};
use std::io::{stdin, Write};
use tokio::signal;

mod stream_duration;
mod workers;

#[derive(Debug, thiserror::Error)]
enum AppError {
    #[error("IO error: {0}")]
    IO(#[from] std::io::Error),
    #[error("HTTP error: {0}")]
    Reqwest(#[from] reqwest::Error),
    #[error("Stream duration error: {0}")]
    StreamDuration(#[from] StreamDurationError),
    #[error("Worker error: {0}")]
    Worker(#[from] workers::WorkerError<Box<dyn std::error::Error + Send + Sync>>),
}

async fn ask_for_value(desc: &str) -> Result<String, AppError> {
    println!("{}", desc);
    let mut buf = String::new();
    stdin().read_line(&mut buf)?;
    Ok(buf.trim().to_string())
}

#[tokio::main]
async fn main() -> Result<(), AppError> {
    info!("Search for clips: https://twitchtracker.com/<streamer>/streams/<stream_id>");
    debug!("Initializing with {} workers", WORKERS);

    let streamer = ask_for_value("Please enter the streamer's username:").await?;
    let stream_id = ask_for_value("Please enter the vod ID:").await?;

    let stream_duration = match get_stream_duration(&stream_id, &streamer).await {
        Ok(duration) => duration,
        Err(StreamDurationError::DurationNotFound) => {
            eprintln!("Stream duration not found. Using default of 8 hours.");
            28800 // 8 hours in seconds
        }
        Err(e) => return Err(AppError::StreamDuration(e)),
    };

    const WORKERS: usize = 2000;
    let client = reqwest::Client::builder()
        .pool_max_idle_per_host(WORKERS)
        .build()?;

    let logger = Builder::from_env(Env::default().default_filter_or("info"))
        .format(|buf, record| writeln!(buf, "{}", record.args()))
        .build();

    let multi = MultiProgress::new();
    LogWrapper::new(multi.clone(), logger).try_init().unwrap();

    let mut workers = Workers::new();
    let pb = multi.add(ProgressBar::new(stream_duration as u64));
    pb.set_style(
        ProgressStyle::default_bar()
            .template(
                "{spinner:.green} [{elapsed_precise}] [{bar:40.cyan/blue}] {pos}/{len} ({eta})",
            )
            .unwrap()
            .progress_chars("## "),
    );

    let (tx, rx) = async_channel::bounded(WORKERS * 2);

    workers.spawn(async move {
        for i in 0..stream_duration {
            let url = format!(
                "https://clips-media-assets2.twitch.tv/{}-offset-{}.mp4",
                stream_id, i
            );
            tx.send(url)
                .await
                .map_err(|e| AppError::IO(std::io::Error::new(std::io::ErrorKind::Other, e)))?;
        }
        Ok(())
    });

    for _ in 0..WORKERS {
        let pb = pb.clone();
        let client = client.clone();
        let rx = rx.clone();
        workers.spawn(async move {
            while let Ok(line) = rx.recv().await {
                match client.get(&line).send().await {
                    Ok(resp) => {
                        pb.inc(1);
                        if resp.status().as_u16() == 200 {
                            info!("{}", line);
                        } else {
                            debug!("Unexpected status code {} for {}", resp.status(), line);
                        }
                    }
                    Err(e) => error!("Error fetching {}: {}", line, e),
                }
            }
            Ok(())
        });
    }

    tokio::select! {
        result = workers.run() => result?,
        _ = signal::ctrl_c() => {
            println!("Received Ctrl+C, shutting down...");
        }
    }

    Ok(())
}
